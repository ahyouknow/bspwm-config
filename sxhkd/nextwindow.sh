#!/bin/sh
window=$(bspc query -N -n next.window)
bspc query -N -d |grep --silent $window || window=$(bspc query -N -n next.local) 
echo $window
