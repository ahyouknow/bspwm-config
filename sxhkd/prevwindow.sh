#!/bin/sh
window=$(bspc query -N -n prev.window)
bspc query -N -d |grep --silent $window || window=$(bspc query -N -n prev.local) 
echo $window
